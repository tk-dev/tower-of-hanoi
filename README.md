# Tower of Hanoi
Tower of Hanoi - Windows Application

## User Interface
![Info Screen](https://github.com/tk-codes/tower_of_hanoi/blob/master/Screen_1_Info.PNG)
![Start Screen](https://github.com/tk-codes/tower_of_hanoi/blob/master/Screen_2_Start.PNG)
![Playing Screen](https://github.com/tk-codes/tower_of_hanoi/blob/master/Screen_3_Play.bmp)
![Game Over](https://github.com/tk-codes/tower_of_hanoi/blob/master/Screen_3_Over.PNG)
