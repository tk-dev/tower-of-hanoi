﻿namespace TowerOfHanoi {
    partial class BrickView {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lbSize = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbSize
            // 
            this.lbSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSize.Location = new System.Drawing.Point(0, 0);
            this.lbSize.Margin = new System.Windows.Forms.Padding(0);
            this.lbSize.Name = "lbSize";
            this.lbSize.Size = new System.Drawing.Size(183, 41);
            this.lbSize.TabIndex = 0;
            this.lbSize.Text = "label1";
            this.lbSize.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbSize.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lbSize_MouseClick);
            // 
            // BrickView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.lbSize);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "BrickView";
            this.Size = new System.Drawing.Size(183, 41);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbSize;
    }
}
