﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TowerOfHanoi.Model;

namespace TowerOfHanoi {
    public partial class Info : Form {
        public Info() {
            InitializeComponent();

            tbName.Text = "Keerthikan";
            cbDiff.SelectedIndex = cbDiff.Items.IndexOf("Easy");
        }

        private void btnStart_Click(object sender, EventArgs e) {
            Game game = new Game(tbName.Text, getDifficulty());
            new GamePanel(game).Show();
            this.Hide();
        }

        private Difficulty getDifficulty() {
            string value = cbDiff.SelectedItem.ToString();
            switch (value) {
                case "Easy":
                    return Difficulty.Easy;
                case "Medium":
                    return Difficulty.Medium;
                default:
                    throw new ArgumentNullException("No value selected");
            }
        }
    }
}
