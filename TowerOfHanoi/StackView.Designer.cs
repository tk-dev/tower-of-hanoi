﻿namespace TowerOfHanoi {
    partial class StackView {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.pBar = new System.Windows.Forms.Panel();
            this.flBricksHolder = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // pBar
            // 
            this.pBar.BackColor = System.Drawing.SystemColors.Desktop;
            this.pBar.Location = new System.Drawing.Point(3, 304);
            this.pBar.Margin = new System.Windows.Forms.Padding(0);
            this.pBar.Name = "pBar";
            this.pBar.Size = new System.Drawing.Size(223, 14);
            this.pBar.TabIndex = 0;
            // 
            // flBricksHolder
            // 
            this.flBricksHolder.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flBricksHolder.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
            this.flBricksHolder.Location = new System.Drawing.Point(10, 18);
            this.flBricksHolder.Name = "flBricksHolder";
            this.flBricksHolder.Size = new System.Drawing.Size(199, 286);
            this.flBricksHolder.TabIndex = 1;
            this.flBricksHolder.MouseClick += new System.Windows.Forms.MouseEventHandler(this.flBricksHolder_MouseClick);
            this.flBricksHolder.MouseEnter += new System.EventHandler(this.flBricksHolder_MouseEnter);
            this.flBricksHolder.MouseLeave += new System.EventHandler(this.flBricksHolder_MouseLeave);
            // 
            // StackView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.flBricksHolder);
            this.Controls.Add(this.pBar);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "StackView";
            this.Size = new System.Drawing.Size(229, 321);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pBar;
        private System.Windows.Forms.FlowLayoutPanel flBricksHolder;
    }
}
