﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerOfHanoi.Model {
    public class Brick {

        // size 1 to (Easy = 3, Medium 4)
        public int size {get; private set;}

        public Color color {get; private set;}
        
        public Brick(int size, Color color) {
            this.size = size;
            this.color = color;
        }
    }
}
