﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerOfHanoi.Model {
    public enum  Difficulty {

        Easy = 3,
        Medium = 4
    }
}
