﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerOfHanoi.Model {
    public class Stack {

        // position 1 to (Easy = 3, Medium 4)
        private int position;

        private List<Brick> bricks;

        public Stack(int pos) {
            this.position = pos;

            bricks = new List<Brick>();
        }

        public List<Brick> getBricks() {
            return this.bricks;
        }

        public void addBrick(Brick b) {
            this.bricks.Add(b);
        }

        public Boolean isValidToBeMoved(Brick b) {
            if (bricks.Last() == b)
                return true;
            else
                return false;
        }

        public Boolean isValidMove(Brick b, int currentPos) {
            // stack can only be next to the current position
            if (currentPos + 1 == position || currentPos - 1 == position) {
                if (bricks.Count == 0)
                    return true;
                else if (bricks.Last().size > b.size)
                    return true;
            }
            return false;
        }

        public int getPosition() {
            return position;
        }
    }
}
