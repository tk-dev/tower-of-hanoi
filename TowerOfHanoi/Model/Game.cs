﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerOfHanoi.Model {
    public class Game {

        public string player { get; private set; }
        public Difficulty difficulty { get; private set; }

        private int moves;

        private Stopwatch stopwatch;

        private List<Stack> stacks;

        private Random random = new Random();

        public Game(string player, Difficulty difficulty) {
            this.player = player;
            this.difficulty = difficulty;

            moves = 0;
            stopwatch = new Stopwatch();

            setupGame();
        }

        private void setupGame() {
            stacks = new List<Stack>();
            int level = (int)difficulty;
            for (int i = 1; i <= level; i++) {

                Stack stack = new Stack(i);
                this.stacks.Add(stack);

                // create bricks
                if (i == 1) {
                    for (int b = level; b >= 1; b--) {
                        Brick brick = new Brick(b, getRandomColor());
                        stack.addBrick(brick);
                    }
                }
            }
        }

        public void increaseMove() {
            moves++;
        }

        public void decreaseMove() {
            moves--;
        }

        public void start() {
            stopwatch.Start();
        }

        public void restart() {
            moves = 0;
            stopwatch.Restart();
        }

        public void stop() {
            stopwatch.Stop();
        }

        public int getMoves() {
            return this.moves;
        }

        public TimeSpan getTime() {
            return stopwatch.Elapsed;
        }

        public Color getRandomColor() {            
            KnownColor[] names = (KnownColor[])Enum.GetValues(typeof(KnownColor));
            KnownColor randomColor = names[random.Next(names.Length)];
            return Color.FromKnownColor(randomColor);
        }

        public List<Stack> getStacks() {
            return this.stacks;
        }

        public int getLevel() {
            return (int)difficulty;
        }


        internal bool isOver() {
            Stack lastStack = stacks.Last();

            if (lastStack.getBricks().Count == getLevel())
                return true;
            else
                return false;
        }
    }
}
