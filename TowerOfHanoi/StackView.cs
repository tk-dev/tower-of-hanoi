﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TowerOfHanoi.Model;

namespace TowerOfHanoi {
    public partial class StackView : UserControl {

        private Stack stack;
        private double availableWidth;
        private int height;

        private GamePanelCallback callback;
        
        public StackView(Stack stack, GamePanelCallback callback, double availableWidth, int height) {
            InitializeComponent();
            this.Height = height;
            this.Width = (int) availableWidth;
            this.callback = callback;
                                    
            this.stack = stack;
            this.availableWidth = availableWidth;
            this.height = height;
        }

        private void flBricksHolder_MouseEnter(object sender, EventArgs e) {
            Brick selectedBrick = callback.getSelectedBrick();
            if (selectedBrick == null) {
                flBricksHolder.BackColor = Color.FromKnownColor(KnownColor.Highlight);
            } else {
                if (stack.isValidMove(selectedBrick, callback.getStackPosOfSelectedBrick())) {
                    flBricksHolder.BackColor = Color.FromKnownColor(KnownColor.GreenYellow);
                } else {
                    flBricksHolder.BackColor = Color.FromKnownColor(KnownColor.Red);
                }
            }
        }

        private void flBricksHolder_MouseLeave(object sender, EventArgs e) {
            flBricksHolder.BackColor = Color.FromKnownColor(KnownColor.Control);
        }

        public void update() {
            flBricksHolder.Controls.Clear();

            int total = stack.getBricks().Count;
            int totalWidth = flBricksHolder.Width;

            foreach (Brick b in stack.getBricks()) {
                int brickWidth = (totalWidth * (10 - total + b.size)) / 10;
                int diff = totalWidth - brickWidth;
                BrickView bView = new BrickView(b, callback, this, brickWidth);
                bView.Margin = new Padding(diff / 2, 0, 0, 0);
                flBricksHolder.Controls.Add(bView);
            }
        }

        public Boolean validSelection(BrickView brick) {
            return stack.isValidToBeMoved(brick.getBrick());
        }

        private void flBricksHolder_MouseClick(object sender, MouseEventArgs e) {
            Brick selectedBrick = callback.getSelectedBrick();
            if (selectedBrick != null) {
                if (stack.isValidMove(selectedBrick, callback.getStackPosOfSelectedBrick())) {
                    // remove it from parent
                    callback.removeBrickFromParent(selectedBrick);
                    // add it to new parent
                    stack.getBricks().Add(selectedBrick);
                    callback.increaseMove();
                    callback.updatePanel();
                }
            }
        }

        public int getStackPos() {
            return stack.getPosition();
        }
    }
}
