﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TowerOfHanoi.Model;

namespace TowerOfHanoi {
    public partial class GamePanel : Form, GamePanelCallback {

        private Game game;

        private List<StackView> stackViews;

        private BrickView selectedBrick;

        private System.Timers.Timer timer;

        public GamePanel(Game game) {
            InitializeComponent();

            this.game = game;

            setupGameView();
            updateGameView();
        }

        private void updateGameView() {
            foreach (StackView sView in stackViews) {
                sView.update();
            }
        }

        private void setupGameView() {
            this.flContainer.Controls.Clear();
            int total = game.getLevel();
            double availableWidth = (flContainer.Width - (total*6)) / total;

            // Create Stack view
            int i = 0;
            stackViews = new List<StackView>();
            foreach (Stack stack in game.getStacks()) {
                i++;
                StackView sView = new StackView(stack, this, availableWidth, flContainer.Height);
                stackViews.Add(sView);

                flContainer.Controls.Add(sView);
            }

            timer = new System.Timers.Timer();
            timer.Interval = 1000;
            timer.Elapsed += timer_Elapsed;
            timer.Start();

            game.start();
        }

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e) {
            Action action = new Action(updateTime);
            this.BeginInvoke(action);
        }

        private void updateTime() {
            TimeSpan ts = game.getTime();
            lbTime.Text = String.Format("{0:00}:{1:00}", ts.Minutes, ts.Seconds);
        }

        public void selectBrick(BrickView brick) {
            if (this.selectedBrick != null) {
                selectedBrick.deselect();
            }

            selectedBrick = brick;                
        }


        public Brick getSelectedBrick() {
            if (selectedBrick == null)
                return null;
            return selectedBrick.getBrick();
        }

        public int getStackPosOfSelectedBrick() {
            return selectedBrick.getCurrentStackPos();
        }


        public bool removeBrickFromParent(Brick b) {
            foreach (Stack s in game.getStacks()) {
                if (s.getBricks().Contains(b)) {
                    s.getBricks().Remove(b);
                    return true;
                }
            }
            return false;
        }

        public void updatePanel() {
            updateGameView();
            selectedBrick.deselect();
            selectedBrick = null;

            if (game.isOver()) {
                game.stop();
                timer.Stop();

                DialogResult result = MessageBox.Show("Game Over");
                Application.Exit();
            }
        }


        public void increaseMove() {
            game.increaseMove();
            lbMoves.Text = game.getMoves().ToString();
        }
    }

    public interface GamePanelCallback {
        void selectBrick(BrickView brick);

        Brick getSelectedBrick();

        Boolean removeBrickFromParent(Brick b);

        void updatePanel();

        int getStackPosOfSelectedBrick();

        void increaseMove();
    }


}
