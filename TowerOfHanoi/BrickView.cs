﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TowerOfHanoi.Model;

namespace TowerOfHanoi {
    public partial class BrickView : UserControl {

        private Brick brick;

        private StackView parent;

        private bool selected = false;

        private GamePanelCallback callback;

        public BrickView(Brick brick, GamePanelCallback callback, StackView parent, int width) {
            InitializeComponent();
            this.callback = callback;
            this.parent = parent;

            this.brick = brick;
            this.Width = width;

            lbSize.Text = brick.size.ToString();
            this.BackColor = brick.color;
        }

        private void lbSize_MouseClick(object sender, MouseEventArgs e) {

            if (parent.validSelection(this)) {
                // toggle selection
                selected = (selected) ? false : true;

                if (selected) {
                    callback.selectBrick(this);
                    this.BackColor = Color.FromKnownColor(KnownColor.LightBlue);
                } else {
                    callback.selectBrick(null);
                    this.BackColor = brick.color;
                }
            } else {
                // Invalid selection
                MessageBox.Show("Invalid Selection");
            }
        }

        public void deselect() {
            selected = false;
            this.BackColor = brick.color;
        }

        public Brick getBrick() {
            return brick;
        }

        public int getCurrentStackPos() {
            return parent.getStackPos();
        }
    }
}
